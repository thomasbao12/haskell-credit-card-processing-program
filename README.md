### What is this repository for? ###

This program will add new credit card accounts, process charges and credits against them, and display summary information, based on input from stdin.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

You'll need a haskell compiler with the requisite libraries.

The simplest way is to get the haskell platform and to use cabal to install the following packages:
- bytestring-trie
- hashtables

There's a shell script called runTests.sh, that runs the testing frameworks
There's another shell script called buildAll.sh, that builds the executable credit-card-simulator

The credit-card-simulator runs via input from stdin

### Why Haskell? ###
Since testing was emphasized, I wanted a static type checked language with good testing frameworks.  

This naturally lead me to Haskell, which has QuickCheck and a highly expressive type system.  

### Design Considerations ###
Names needed to be traversed in order at the end, so I chose a Trie rather than a Map.
I chose a HashTable for the dictionary of card numbers and balances.

I tried to keep functions and types small and build on smaller pieces, which is standard haskell style.  

It also made testing easier.  

I didn't test Accounts with quickcheck since it's the union of two types - one built in and one already quick checked.

I have test scripts you can run.  runTests.sh.

NOTE: From e-mails I gathered that names would map surjectively to card numbers.  
So multiple people would have the same card, but multiple people would not have multiple cards.
Based on that information, we do not overwrite existing cards.  
Ex)
Add Thomas 42 $100
Charge Thomas $100
Add Sally  42 $200
results in 
Thomas: $100
Sally: $100


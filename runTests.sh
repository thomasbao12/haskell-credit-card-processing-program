echo "***********************"
echo "testing Luhn10.hs"
echo "***********************"
runghc -isrc src/TestLuhn10.hs -odir build

echo "***********************"
echo "testing CardBalances.hs"
echo "***********************"
runghc -isrc src/TestCardBalances.hs -odir build

echo "***********************"
echo "running test samples"
echo "***********************"

./build.sh
cat test-samples/testInput.txt | ./creditCardSimulator &> test-samples/testOutput.txt
cat test-samples/testInput2.txt | ./creditCardSimulator &> test-samples/testOutput2.txt
cat test-samples/testInput3.txt | ./creditCardSimulator &> test-samples/testOutput3.txt
cat test-samples/testInput4.txt | ./creditCardSimulator &> test-samples/testOutput4.txt
cat test-samples/testInput5.txt | ./creditCardSimulator &> test-samples/testOutput5.txt

echo "***********************"
echo "diffs between test sample 1"
echo "***********************"
diff test-samples/output.txt test-samples/testOutput.txt 

echo "***********************"
echo "diffs between test sample 2"
echo "***********************"
diff test-samples/output2.txt test-samples/testOutput2.txt 

echo "***********************"
echo "diffs between test sample 3"
echo "***********************"
diff test-samples/output3.txt test-samples/testOutput3.txt 

echo "***********************"
echo "diffs between test sample 4"
echo "***********************"
diff test-samples/output4.txt test-samples/testOutput4.txt 

echo "***********************"
echo "diffs between test sample 5"
echo "***********************"
diff test-samples/output5.txt test-samples/testOutput5.txt 

import Test.QuickCheck 
import Test.QuickCheck.Monadic (assert, monadicIO, pick, pre, run)
import CardBalances 
import qualified Luhn10 (checkSums)
--import qualified Data.HashTable.IO as HashTable -- requires cabal install hashtables
import Data.Maybe (fromJust, isJust, Maybe(..))
import Control.Monad (when) 

instance Arbitrary Balance where
	arbitrary = do
		amount <- arbitrary :: Gen Integer
		limit  <- choose (0, 1000)
		return $ Balance (min amount limit) limit
		
prop_limitedAmount :: Balance -> Bool
prop_limitedAmount balance = (amount balance) <= (limit balance)

test_emptyBalance :: Integer -> Bool
test_emptyBalance limit 
	| limit < 0 = True
	| otherwise = amount (emptyBalance limit) == 0

test_charge :: Integer -> Balance -> Bool 
test_charge debit balance =
	amount newBalance == if (debit + (amount balance) <= limit balance) then (debit + (amount balance)) else (amount balance)
	where
		newBalance = charge debit balance

test_credit :: Integer -> Balance -> Bool 
test_credit delta = test_charge (-delta)

test_createCard :: Integer -> Property
test_createCard cardNum = monadicIO $ do
	cardBalances <- run emptyCardBalances
	run $ createCard cardNum 100 cardBalances
	maybeBalance <- run $ lookupByCardNum cardBalances cardNum
	assert $ (Luhn10.checkSums cardNum) == (isJust maybeBalance)

test_chargeCard :: Integer -> Integer -> Property
test_chargeCard debit limit = monadicIO $ do
	cardBalances <- run emptyCardBalances
	cardNum <- run $ return 42
	actualLimit <- run $ return $ max 0 limit
	run $ createCard cardNum actualLimit cardBalances
	run $ chargeCard cardNum debit cardBalances
	maybeBalance <- run $ lookupByCardNum cardBalances cardNum
	balanceAmount <- run $ return $ amount $ fromJust maybeBalance
	assert $ if (debit <= actualLimit)
		then 
			balanceAmount == debit
		else
			balanceAmount == 0

test_creditCard credit = test_chargeCard (-credit)

test_multiCreateCard :: Integer -> Integer -> Property
test_multiCreateCard charge limit = monadicIO $ do
	cardBalances <- run emptyCardBalances
	run $ createCard 42 (max 0 charge) cardBalances -- limit = 0 or charge if charge > 0
	run $ chargeCard 42 charge cardBalances
	run $ createCard 42 0 cardBalances 
	maybeBalance <- run $ lookupByCardNum cardBalances 42
	assert $ (amount $ fromJust maybeBalance) ==  charge

main = do
	quickCheck prop_limitedAmount
	quickCheck test_emptyBalance
	quickCheck test_charge
	quickCheck test_credit
	quickCheck test_createCard
	quickCheck test_chargeCard
	quickCheck test_creditCard
	quickCheck test_multiCreateCard
import qualified Luhn10 (checkSums)
import Test.HUnit

validCardNums = [4111111111111111, 5454545454545454, 4460397770029622, -4460397770029622]
invalidCardNums = [1234567890123456, 545454545, 4460397770029627, -4460397770029627]

testValidNum cardNum = TestCase $ assertBool (show cardNum ++ " should checksum") (Luhn10.checkSums cardNum) 
testInvalidNum cardNum = TestCase $ assertBool (show cardNum ++ " should not checksum") (not $ Luhn10.checkSums cardNum) 

positiveTestCases = TestList $ map testValidNum validCardNums
negativeTestCases = TestList $ map testInvalidNum invalidCardNums

main :: IO Counts
main = runTestTT $ TestList [positiveTestCases, negativeTestCases]   


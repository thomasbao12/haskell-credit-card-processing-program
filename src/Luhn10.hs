module Luhn10 (checkSums) where

sumDigits :: Integer->Integer
sumDigits 0 = 0
sumDigits i 
	| i < 0 	= sumDigits (-i)
	| otherwise = i `mod` 10 + sumDigits (i `div` 10) 
	
luhnSum :: Integer->Integer
luhnSum 0 = 0
luhnSum x 
	| x < 0 	= luhnSum (-x)
	| otherwise = d1 + sumDigits (2 * d2) + luhnSum (x `div` 100)
    where
   	 d1 = x `mod` 10
   	 d2 = (x `div` 10) `mod` 10

checkSums :: Integer->Bool
checkSums x = ((luhnSum x) `mod` 10) == 0

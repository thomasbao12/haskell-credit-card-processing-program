module Accounts (Accounts, emptyAccounts, printAccount, addNameAndCardBalance, chargeByName, creditByName) where 

import qualified Data.Trie as Trie (Trie, keys, lookup, insert, empty) -- requires cabal install bytestring-trie
import Data.ByteString.Char8 (pack, unpack)
import CardBalances (CardBalances, amount, emptyCardBalances, createCard, chargeCard, creditCard, lookupByCardNum)
import Data.Maybe (fromJust, isJust)
import Control.Monad (when) 

-- luhn 10 invalid card numbers are in the trie but not in cardBalances
type NameCards = Trie.Trie Integer
type Accounts = (NameCards, CardBalances)

emptyAccounts = do
	nameCards <- return Trie.empty :: IO NameCards
	cardBalances <- emptyCardBalances
	return (nameCards, cardBalances)

lookupBalanceByName :: Accounts -> String -> IO String
lookupBalanceByName (nameCards, cardBalances) name = do
	cardNum <- return $ fromJust $ Trie.lookup (pack name) nameCards
	maybeBalance <- lookupByCardNum cardBalances cardNum
	if (isJust maybeBalance) 
		then return $ "$" ++ (show $ amount $ fromJust $ maybeBalance)
	else
		return "error"

printAccount :: Accounts -> IO ()
printAccount accounts@(nameCards, cardBalances) = do
	names <- return $ map unpack $ Trie.keys nameCards
	balances <- mapM (lookupBalanceByName accounts) names
	mapM_ putStrLn (zipWith (\name balanceStr -> name ++ ": " ++ balanceStr) names balances)
	
addNameAndCard :: String -> Integer -> NameCards -> NameCards
addNameAndCard name = Trie.insert (pack name)

addNameAndCardBalance :: Accounts -> String -> Integer -> Integer -> IO Accounts
addNameAndCardBalance (nameCards, cardBalances) name cardNum limit = do
	createCard cardNum limit cardBalances
	return (addNameAndCard name cardNum nameCards, cardBalances)
	
chargeByName :: Accounts -> String -> Integer -> IO Accounts
chargeByName (nameCards, cardBalances) name delta = do 
	when (isJust maybeCardNum) $ chargeCard (fromJust maybeCardNum) delta cardBalances
	return (nameCards, cardBalances)
	where 
		maybeCardNum = Trie.lookup (pack name) nameCards 

creditByName accounts name delta = chargeByName accounts name (-delta)
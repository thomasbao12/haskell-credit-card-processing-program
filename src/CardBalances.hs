module CardBalances (CardBalances, Balance (Balance), amount, limit, emptyBalance, charge, credit, emptyCardBalances, createCard, chargeCard, creditCard, lookupByCardNum) where
import qualified Luhn10 (checkSums)
import qualified Data.HashTable.IO as HashTable -- requires cabal install hashtables
import Control.Monad (when)
import Data.Maybe (fromJust, isJust, isNothing, Maybe(..))

data Balance = Balance { amount :: Integer, limit :: Integer } deriving Show

emptyBalance :: Integer -> Balance 
emptyBalance limit  
	| limit < 0 = error "cannot create balance with limit < 0"
	| otherwise = Balance 0 limit

charge :: Integer -> Balance -> Balance
charge debit balance = if (newAmount <= limit balance) then balance { amount = newAmount } else balance
	where
		newAmount = (debit + (amount balance))

credit :: Integer -> Balance -> Balance
credit amt = charge (-amt)

type CardBalances = HashTable.BasicHashTable Integer Balance

emptyCardBalances :: IO CardBalances
emptyCardBalances = HashTable.new

lookupByCardNum :: CardBalances -> Integer -> IO (Maybe Balance)
lookupByCardNum = HashTable.lookup 

-- do not overwrite existing accounts
createCard :: Integer -> Integer -> CardBalances -> IO()
createCard cardNum limit cardBalances = do
	maybeBalance <- HashTable.lookup cardBalances cardNum
	when (Luhn10.checkSums cardNum && isNothing maybeBalance) $ HashTable.insert cardBalances cardNum (emptyBalance limit)

chargeCard :: Integer -> Integer -> CardBalances -> IO()
chargeCard cardNum delta cardBalances = do
	maybeBalance <- HashTable.lookup cardBalances cardNum
	when (isJust maybeBalance) $ 
		HashTable.insert cardBalances cardNum (charge delta $ fromJust maybeBalance)

creditCard cardNum delta = chargeCard cardNum (-delta)
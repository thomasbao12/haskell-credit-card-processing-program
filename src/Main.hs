import Control.Monad (foldM)
import Accounts (emptyAccounts, Accounts, printAccount, addNameAndCardBalance, chargeByName, creditByName)

-- assume all input is properly formatted
parseCommand :: Accounts -> String -> IO Accounts
parseCommand accounts commandStr = 
	case command of
		"Add" 		-> addNameAndCardBalance accounts name cardNum amount 
		"Charge" 	-> chargeByName accounts name amount
		"Credit"    -> creditByName accounts name amount
		otherwise	-> error "unknown command"
	where
		args 	= words commandStr
		command = head args
		name 	= args !! 1
		cardNum = read $ args !! 2 :: Integer
		amount 	= read $ tail $ last args :: Integer
		
main = do 
	contents <- getContents
	commands <- return $ lines contents
	initAccount <- emptyAccounts
	result <- foldM parseCommand initAccount commands
	printAccount result
